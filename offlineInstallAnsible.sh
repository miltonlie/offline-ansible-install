#!/bin/bash

RPM_DIR="/home/milton/yum/ansible"

# Capture install order
dep_array=("python2-pyasn1-0.1.9-7.el7.noarch.rpm"
           "python-ipaddress-1.0.16-2.el7.noarch.rpm"
           "python-six-1.9.0-2.el7.noarch.rpm"
           "python-httplib2-0.9.2-1.el7.noarch.rpm"
           "sshpass-1.06-2.el7.x86_64.rpm"
           "libyaml-0.1.4-11.el7_0.x86_64.rpm"
           "PyYAML-3.10-11.el7.x86_64.rpm"
           "python-backports-1.0-8.el7.x86_64.rpm"
           "python-backports-ssl_match_hostname-3.5.0.1-1.el7.noarch.rpm"
           "python-setuptools-0.9.8-7.el7.noarch.rpm"
           "python-babel-0.9.6-8.el7.noarch.rpm"
           "python-ply-3.4-11.el7.noarch.rpm"
           "python-pycparser-2.14-1.el7.noarch.rpm"
           "python-cffi-1.6.0-5.el7.x86_64.rpm"
           "python-markupsafe-0.11-10.el7.x86_64.rpm"
           "python-jinja2-2.7.2-4.el7.noarch.rpm"
           "python-idna-2.4-1.el7.noarch.rpm"
           "python-enum34-1.0.4-1.el7.noarch.rpm"
           "python2-cryptography-1.7.2-2.el7.x86_64.rpm"
           "python-paramiko-2.1.1-9.el7.noarch.rpm"
           "libtirpc-0.2.4-0.16.el7.x86_64.rpm"
           "python2-pip-8.1.2-12.el7.noarch.rpm"
           "ansible-2.8.3-1.el7.ans.noarch.rpm")

# Install the RPMs
for i in ${dep_array[@]}; do
    if [ ! -f ${RPM_DIR}/${i} ]; then
        echo ${RPM_DIR}/${i} does NOT exist
        exit
    fi
    sudo yum install --nogpgcheck -y ${RPM_DIR}/${i}
done
