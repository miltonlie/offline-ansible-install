#!/bin/bash

RPM_DIR="/home/milton/yum/ansible"

# Capture install order
dep_array=("python2-pyasn1"
           "python-ipaddress"
           "python-six"
           "python-httplib2"
           "sshpass"
           "libyaml"
           "PyYAML"
           "python-backports"
           "python-backports-ssl_match_hostname"
           "python-setuptools"
           "python-babel"
           "python-ply"
           "python-pycparser"
           "python-cffi"
           "python-markupsafe"
           "python-jinja2"
           "python-idna"
           "python-enum34"
           "python2-cryptography"
           "python-paramiko"
           "libtirpc"
           "python2-pip"
           "ansible")

# Install the RPMs
for i in ${dep_array[@]}; do
    sudo yum remove -y ${i}
done
